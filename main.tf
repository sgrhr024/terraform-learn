provider "aws" {
     
}

resource "aws_vpc" "FirstDemo" {
  cidr_block = var.vpc_cidr_block
  tags = {
      Name: "TerraVPC"
      "DemoPurpose" = "Demo"}   
}

variable "subnet_cidr_block" {
   description = "subnet cidr blocks and name tags for vpc"
   type = list(object({
     cidr_block = string
     name = string
   })) 
}

variable "vpc_cidr_block" {
   description = "vpc cidr block"
}


resource "aws_subnet" "PUBLIC" {
  vpc_id = aws_vpc.FirstDemo.id
  cidr_block = var.subnet_cidr_block[0].cidr_block
  availability_zone = "us-east-2c"
  tags = {
      Name: var.cidr_block(0).name
  }
}

resource "aws_subnet" "PRIVATE" {
  vpc_id = aws_vpc.FirstDemo.id
  cidr_block = var.subnet_cidr_block[1].cidr_block
  availability_zone = "us-east-2b"
  tags = {
      Name: var.cidr_block(1).name
  }
}








































































